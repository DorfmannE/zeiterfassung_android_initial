package com.example.zeiterfassung_modulo3;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;

import static android.content.Context.MODE_PRIVATE;
import static android.database.sqlite.SQLiteDatabase.openOrCreateDatabase;


public class Exports {
    File localTimetrackingFile;
    String timeStart;
    String timeEnd;
    String pause;

    SQLiteDatabase mydatabase;

    public Exports(){

    }

    public Exports(String timeStart, String timeEnd, String pause){
        this.timeStart=timeStart;
        this.timeEnd=timeEnd;
        this.pause=pause;


    }


    public void localExport() {



        mydatabase.execSQL("CREATE TABLE IF NOT EXISTS workingTimeSheet(date VARCHAR, workStart VARCHAR, workEnd VARCHAR, pause VARCHAR)");
        mydatabase.execSQL("INSERT INTO workingTimeSheet (workStart, workEnd, pause) VALUES ("+timeStart+","+timeEnd+","+pause+")");
        Log.i("Message", "writing in the database successful");






    }


}

