package com.example.zeiterfassung_modulo3;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.io.IOException;
import java.sql.Timestamp;

public class TimeTracking extends AppCompatActivity {
//Variablen für GoogleSignIn
    //ImageView imageView;
    TextView name, email, id;
    Button signOut;
    GoogleSignInClient mGoogleSignInClient;

//Varibalen für die Zeiterfassung
boolean beginOfTheDay=true;
    boolean working =false;
    boolean longClick=false;
    TextView startWorking;
    TextView stopWorking;
    TextView pause;
    TextView workingTime;
    Button startStopButton;
    Timestamp timestampStart;
    Timestamp timestampEnd;
    long fullPause=0;
    long fullWorkingTime=0;
    long difference=0;
    int hours=0;
    int minutes=0;
    int seconds=0;
    String Stringhours="00";
    String Stringminutes="00";
    String Stringseconds="00";




//Funktion für Start/Stop-Button
public void buttonClicked (View view){

    if (working == false) {
        timestampStart = new Timestamp(System.currentTimeMillis());
        Log.i("Moment of clicking", timestampStart.toString());

        working = true;

        startStopButton.setText("STOP WORK");
        if (beginOfTheDay) {
            startWorking = findViewById(R.id.timeStartTextView);
            startWorking.setText("Work started: " + timestampStart.toString());

        }
        beginOfTheDay = false;

        if (timestampEnd != null) {
            difference = timestampStart.getTime() - timestampEnd.getTime();
            fullPause = fullPause + difference;
            pause = findViewById(R.id.pauseTextView);

            hours = (int) fullPause / 1000 / 3600;
            minutes = ((int) fullPause - (hours * 3600 * 1000)) / 1000 / 60;
            seconds = ((int) fullPause - (hours * 3600 * 1000) - (minutes * 1000 * 60)) / 1000;

            if (hours < 10) {
                Stringhours = "0" + Integer.toString(hours);
            } else {
                Stringhours = Integer.toString(hours);
            }
            if (minutes < 10) {
                Stringminutes = "0" + Integer.toString(minutes);
            } else {
                Stringminutes = Integer.toString(minutes);
            }
            if (seconds < 10) {
                Stringseconds = "0" + Integer.toString(seconds);
            } else {
                Stringseconds = Integer.toString(seconds);
            }

            pause.setText("Pause:              " + Stringhours + "h " + Stringminutes + "min " + Stringseconds + "sec");
        }


    } else {
        timestampEnd = new Timestamp(System.currentTimeMillis());
        Log.i("Moment of clicking", timestampEnd.toString());

        working = false;

        startStopButton.setText("press to Start Work \n\nor\n\n Press and hold to export");

        stopWorking = findViewById(R.id.timeEndTextView);
        stopWorking.setText("Work ended: " + timestampEnd.toString());

        difference = timestampEnd.getTime() - timestampStart.getTime();
        fullWorkingTime = fullWorkingTime + difference;
        workingTime = findViewById(R.id.workTextView);

        hours = (int) fullWorkingTime / 1000 / 3600;
        minutes = ((int) fullWorkingTime - (hours * 3600 * 1000)) / 1000 / 60;
        seconds = ((int) fullWorkingTime - (hours * 3600 * 1000) - (minutes * 1000 * 60)) / 1000;

        if (hours < 10) {
            Stringhours = "0" + Integer.toString(hours);
        } else {
            Stringhours = Integer.toString(hours);
        }
        if (minutes < 10) {
            Stringminutes = "0" + Integer.toString(minutes);
        } else {
            Stringminutes = Integer.toString(minutes);
        }
        if (seconds < 10) {
            Stringseconds = "0" + Integer.toString(seconds);
        } else {
            Stringseconds = Integer.toString(seconds);
        }
        workingTime.setText("Working Time: " + Stringhours + "h " + Stringminutes + "min " + Stringseconds + "sec");


    }

}


//On Create Section______________________________________________________________________________________________________________________________________
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_tracking);

//Funktion für Start/Stop und Export
        startStopButton = findViewById(R.id.startStopButton);

        startStopButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
//Exporting the Time
                Log.i("to export", startWorking.getText().toString() + " " + stopWorking.getText().toString() + " " + pause.getText().toString());
                Exports workDay= new Exports(timestampStart.toString(), timestampEnd.toString(),Long.toString(fullPause));
                localExport();

//Reseting the TimeTracking
                startStopButton.setText("press to Start Work \n\nor\n\n Press and hold to export");
                startWorking.setText("Work started:");
                stopWorking.setText("Work ended:");
                pause.setText("Pause:              00h 00min 00sec");
                workingTime.setText("Working Time: 00h 00min 00sec");
                beginOfTheDay = true;
                working = false;
                fullPause = 0;
                fullWorkingTime = 0;
                difference = 0;
                hours = 0;
                minutes = 0;
                seconds = 0;
                Stringhours = "00";
                Stringminutes = "00";
                Stringseconds = "00";
                longClick = false;
                timestampStart=null;
                timestampEnd=null;

                return true;
            }
        });
//__________________________________________________________________________________


        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        name=findViewById(R.id.textName);
        email=findViewById(R.id.textEmail);
        signOut=findViewById(R.id.button);
        signOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.button:
                        signOut();
                        break;
                }
            }
        });

        GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(this);
        if (acct != null) {
            String personName = acct.getDisplayName();
            String personEmail = acct.getEmail();
            String personId = acct.getId();
            Uri personPhoto = acct.getPhotoUrl();

            name.setText(personName);
            email.setText(personEmail);
            //id.setText(personId);
            //Glide.with(this).load(String.valueOf(personPhoto)).into(imageView);
        }
    }

    private void signOut() {
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Toast.makeText(TimeTracking.this, "Logout successfull", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                });
    }
    //Zuschneiden vom TimeStamp und lokaler Export auf Gerät in form von SQLite
    public void localExport(){
    String dateString = timestampStart.toString().substring(0,10);
    String workStartString = timestampStart.toString().substring(11,19);
    String workEndString = timestampEnd.toString().substring(11,19);
    double pauseDouble = Double.valueOf(fullPause/1000/3600);
    double pauseDoubleRounded = round(pauseDouble,2);
    String pauseString = (Double.toString(pauseDoubleRounded));
        try {
            SQLiteDatabase database = this.openOrCreateDatabase("workingDay", MODE_PRIVATE, null);
 //dont forget to delete "DELETE-DATABASE line below!!!!
                    // database.delete("workingTimeSheet",null,null);
 //____________________________________________________________________________________________
            
            database.execSQL("CREATE TABLE IF NOT EXISTS workingTimeSheet(date VARCHAR, workStart VARCHAR, workEnd VARCHAR, pause VARCHAR)");
            database.execSQL("INSERT INTO workingTimeSheet (date, workStart, workEnd, pause) VALUES ('"+dateString+"', '"+workStartString+"', '"+workEndString+"', '"+pauseString+"')");
            Log.i("Message", "writing in the database successful");

            Cursor c = database.rawQuery("SELECT * FROM workingTimeSheet", null);
            int startIndex = c.getColumnIndex("workStart");
            int endIndex = c.getColumnIndex("workEnd");
            int pauseIndex = c.getColumnIndex("pause");
            if (c.moveToFirst()) {
                do {
                    Log.i("start", c.getString(startIndex));
                    Log.i("end", c.getString(endIndex));
                    Log.i("pause", c.getString(pauseIndex));

                } while (c.moveToNext());
            }
        }catch(Exception e){
            e.printStackTrace();
        }


    }
//Rounding of Double figures on two digits for calculation of the pause in the right format
    public double round(double zahl, int stellen) {
        return (double) ((int)zahl + (Math.round(Math.pow(10,stellen)*(zahl-(int)zahl)))/(Math.pow(10,stellen)));
    }
}